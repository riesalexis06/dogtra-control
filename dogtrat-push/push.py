#!/usr/bin/python2
#
#  push.py
#  dogtrat-push
#

from __future__ import print_function, unicode_literals
from proton import Message
from proton.handlers import MessagingHandler
from proton.reactor import Container

import collections
import json
from apns2.client import APNsClient
from apns2.payload import Payload
from apns2.credentials import TokenCredentials

class ApplePushNotification:
    def __init__(self, topic, token, keyid, teamid, authkeyfile):
        super(ApplePushNotification, self).__init__()
        self.topic = topic
        self.mydevicetoken = token
        auth_key_path = authkeyfile
        self.token_credentials = TokenCredentials(auth_key_path=authkeyfile, auth_key_id=keyid, team_id=teamid)

    def sendPushMessage(self, msg):
        print("send push notification : %s " % msg)
        Notification = collections.namedtuple('Notification', ['token', 'payload'])
        payload = Payload(alert=msg, sound="default", badge=1)
        notifications = [Notification(payload=payload, token=self.mydevicetoken)]
        client = APNsClient(credentials=self.token_credentials, use_sandbox=True)
        client.send_notification_batch(notifications=notifications, topic=self.topic)

class BusMessage(MessagingHandler):
    def __init__(self, topic, token, keyid, teamid, authkeyfile, user, password, server, address):
        super(BusMessage, self).__init__()
        self.server = server
        self.address = address
        self.user = user
        self.password = password
        self.expected = 0
        self.received = 0
        self.last_id = ""

        self.pusher = ApplePushNotification(
            topic=topic,
            token=token,
            keyid=keyid,
            teamid=teamid,
            authkeyfile=authkeyfile
        )

    def on_start(self, event):
        conn = event.container.connect(
            url=self.server,
            user=self.user,
            password=self.password,
            sasl_enabled=True,
            allow_insecure_mechs=True,
            heartbeat=10000
        )
        event.container.create_receiver(conn, self.address)

    def on_message(self, event):
        print("message from %s : %s" % (event.message.id, event.message.body))
        if event.message.id == self.last_id:
            print("duplicate !")
            return
        self.last_id = event.message.id
        if self.expected == 0 or self.received < self.expected:
            message = json.loads(event.message.body)
            if 'type' not in message:
                return
            if message["type"] != "alert":
                return
            alert = message["message"]
            average = message["average"]
            pushMessage = "%s - %s" % (alert, average)
            self.pusher.sendPushMessage(pushMessage)
            self.received += 1
            if self.received == self.expected:
                event.receiver.close()
                event.connection.close()

Container(BusMessage(
    topic="com.alexisries.dogtrat-ios",
    token="IOS_TOKEN",
    keyid="IOS_KEYID",
    teamid="IOS_TEAMID",
    authkeyfile="./auth-key.pem",
    user="dogtratnotif",
    password="MQ_PASSWORD",
    server="dogtrat-gstreamer_activemq_1:5672",
    address="topic://dogtrat/notifications")).run()
