/*
*  main.cpp
*  dogtrat-esp32
*
*  Created by Alexis RIES on 22/02/2020.
*/

#include "WiFi.h"
#include <WiFiClientSecure.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

#define WIFI_SSID "wifi-name"
#define WIFI_PASSWORD "wifi-password"

#define MQTT_ENDPOINT "mq.domain.fr"
#define MQTT_PORT 31883
#define MQTT_USER "dogtratesp"
#define MQTT_PASSWORD "mq-password"
#define MQTT_CLIENT_NAME "dogtratClient"
#define MQTT_MAX_PACKET_SIZE 512

#define NTP_SERVER "pool.ntp.org"
#define GMT_OFFSET_SEC 3600
#define DAYLIGHT_OFFSET_SEC 3600

#define DOGTRA_VIBRATE_PIN 17
#define DOGTRA_VIBRATE_SHORT_DELAY 1000
#define DOGTRA_VIBRATE_LONG_DELAY 4000

/*
#define DOGTRA_STIMULATE_PIN 5
#define DOGTRA_STIMULATE_SHORT_DELAY 300
#define DOGTRA_STIMULATE_LONG_DELAY 700
 */

char publishTopic[]   = "dogtrat.notifications";
char publishPayload[MQTT_MAX_PACKET_SIZE];
const char *subscribeTopic[1] = {
        "dogtrat.command"
};

extern const uint8_t mqtt_root_ca_pem_start[] asm("_binary_certs_ca_pem_start");
extern const uint8_t certificate_pem_start[] asm("_binary_certs_cert_pem_start");
extern const uint8_t private_key_start[] asm("_binary_certs_private_key_start");

int current_state = 0;
int last_origin = 0;
int last_action = 0;

WiFiClientSecure net;
PubSubClient client(net);

void publish_message(bool remove_desired = false)
{
    auto json_desired = "";
    if (remove_desired) {
        json_desired = ",\"desired\": null";
    }
    Serial.print("publish: ");
    sprintf(publishPayload, R"({"state":{"reported":{"action":%d,"last_action":%d,"last_origin":%d}%s},"clientToken":"%s"})",
            current_state,
            last_action,
            last_origin,
            json_desired,
            MQTT_CLIENT_NAME
    );
    Serial.println(publishPayload);
    client.publish(publishTopic, publishPayload);
}

void high_and_low(int new_state, int pin, int wait_delay)
{
    last_action = current_state;
    current_state = new_state;
    publish_message(true);
    Serial.print("setting pin to HIGH: ");
    Serial.println(pin);
    digitalWrite(pin, HIGH);

    delay(wait_delay);

    Serial.print("setting pin to LOW: ");
    Serial.println(pin);
    digitalWrite(pin, LOW);
    last_action = current_state;
    current_state = 0;
    publish_message();
}

void execute_action(int desired_state)
{
    bool update_state = false;
    int pin = 0;
    int delay = 0;
    if (desired_state == 1)
    {
        update_state = true;
        pin = DOGTRA_VIBRATE_PIN;
        delay = DOGTRA_VIBRATE_SHORT_DELAY;
    }
    else if (desired_state == 2)
    {
        update_state = true;
        pin = DOGTRA_VIBRATE_PIN;
        delay = DOGTRA_VIBRATE_LONG_DELAY;
    }
    /*
    else if (desired_state == 3)
    {
        update_state = true;
        pin = DOGTRA_STIMULATE_PIN;
        delay = DOGTRA_STIMULATE_SHORT_DELAY;
    }
    else if (desired_state == 4)
    {
        update_state = true;
        pin = DOGTRA_STIMULATE_PIN;
        delay = DOGTRA_STIMULATE_LONG_DELAY;
    }
    */
    if (update_state)
    {
        high_and_low(desired_state, pin, delay);
    }
}

void setup_wifi()
{
    Serial.print("connecting to ");
    Serial.print(WIFI_SSID);
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    while (WiFiClass::status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println();
    Serial.print("WiFi connected: ");
    Serial.println(WiFi.localIP());
}

void setup_time()
{
    Serial.print("connecting to ");
    Serial.println(NTP_SERVER);
    configTime(GMT_OFFSET_SEC, DAYLIGHT_OFFSET_SEC, NTP_SERVER);

    struct tm time_info{};
    getLocalTime(&time_info);
    Serial.print("ntp time: ");
    Serial.println(&time_info, "%A, %B %d %Y %H:%M:%S");
}

void load_mqtt_certificates()
{
    net.setCACert((const char *)mqtt_root_ca_pem_start);
    net.setCertificate((const char *)certificate_pem_start);
    net.setPrivateKey((const char *)private_key_start);
}

void callback(char* topic, byte* payload, unsigned int length) {
    char buf[MQTT_MAX_PACKET_SIZE];
    strncpy(buf, (const char *)payload, length);
    buf[length] = '\0';
    printf("Message arrived [%s] %s\r\n", topic, buf);
    if (strstr(topic, "dogtrat/command") != nullptr) {
        StaticJsonDocument<200> doc;
        deserializeJson(doc, payload);
        if (doc["state"].containsKey("desired")) {
            if (doc["state"]["desired"].containsKey("action")) {
                last_origin = doc["state"]["desired"]["last_origin"];
                execute_action(doc["state"]["desired"]["action"]);
            }
        }
    }
}

void setup_mqtt()
{
    load_mqtt_certificates();
    Serial.println("connecting to MQTT");
    client.setServer(MQTT_ENDPOINT, MQTT_PORT);
    client.setCallback(callback);
    Serial.println("MQTT Connected!");
}

void reconnect()
{
    while (!client.connected()) {
        Serial.print("attempting MQTT connection...");
        if (client.connect(MQTT_CLIENT_NAME, MQTT_USER, MQTT_PASSWORD)) {
            Serial.println("connected");
            for (auto & i : subscribeTopic) {
                Serial.print("subscribe to ");
                Serial.println(i);
                client.subscribe(i);
            }
            publish_message();
        } else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            delay(5000);
        }
    }
}

void setup()
{
    Serial.begin(115200);
    Serial.setDebugOutput(true);
    Serial.println();
    pinMode(DOGTRA_VIBRATE_PIN, OUTPUT);
    pinMode(DOGTRA_STIMULATE_PIN, OUTPUT);
    setup_wifi();
    setup_time();
    setup_mqtt();
    delay(10);
}

void loop()
{
    if (!client.connected()) {
        reconnect();
    }
    client.loop();
    delay(100);
}
