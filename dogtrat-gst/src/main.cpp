/*
 *  main.cpp
 *  dogtrat-gst
 *
 *  Created by Alexis RIES on 22/02/2020.
 */

#include <cmath>
#include <gst/gst.h>
#include <glib.h>
#include <cstdio>
#include <unistd.h>
#include <iostream>
#include <proton/connection.hpp>
#include <proton/connection_options.hpp>
#include <proton/container.hpp>
#include <proton/delivery.hpp>
#include <proton/message.hpp>
#include <proton/messaging_handler.hpp>
#include <proton/tracker.hpp>
#include <proton/reconnect_options.hpp>
#include "fake_cpp11.hpp"

#define RTSP_LOCATION "rtsp://camera-ip/Streaming/Channels/101"
#define RTSP_USER "dogtrat"
#define RTSP_PASSWORD "camera-password"
#define RTSP_LATENCY 100

#define ACTIVEMQ_LOCATION "dogtrat-gstreamer_activemq_1:5672/topic://dogtrat/notifications"
#define ACTIVEMQ_USER "dogtratgs"
#define ACTIVEMQ_PASSWORD "MQPASSWORD"

#define RECORDING_AUDIO_RMS 0.1


using proton::connection_options;

class messager : public proton::messaging_handler {
  private:
    std::string url;
    std::string addr;
    std::string user;
    std::string password;
    proton::sender sender;
    std::string message;
    int sent;
    int confirmed;
    int total;
    
    bool reconnect;
    bool sasl;
    std::string mechs;
    bool insecure;
    
    bool sended;
    
  public:
    messager(const std::string m, const std::string &s, const std::string &u, const std::string &p, bool r, bool sa, const std::string& ms, bool in) :
        message(m), url(s), user(u), password(p), reconnect(r), sasl(sa), mechs(ms), insecure(in) {}
    void on_container_start(proton::container &c) OVERRIDE {
        proton::connection_options co;
        if (!user.empty()) co.user(user);
        if (!password.empty()) co.password(password);
        if (reconnect) co.reconnect(proton::reconnect_options());
        if (sasl) co.sasl_enabled(true);
        if (!mechs.empty()) co.sasl_allowed_mechs(mechs);
        if (insecure) co.sasl_allow_insecure_mechs(true);
        sender = c.open_sender(url, co);
        sended = false;
    }
    void on_sendable(proton::sender &s) OVERRIDE {
        if (!sended) {
            proton::message msg;
            //msg.id(1);
            msg.body(message);
            s.send(msg);
            sended = true;
        }
    }
    void on_tracker_accept(proton::tracker &t) OVERRIDE {
        g_print("message confirmed !\n");
        t.connection().close();
    }
};

/* Structures to contain all our information, so we can pass it to callbacks */
typedef struct _WatcherElements {
    GstElement *pipeline;
    GstElement *rtspsrc;
    GstElement *rtph264depay;
    GstElement *h264parse;
    GstElement *videotee;
    GstPad     *videoteepad;
    GstElement *avdec_h264;
    GstElement *videoqueue0;
    GstElement *videoconvert;
    GstElement *video_sink;
    GstElement *rtppcmadepay;
    GstElement *alawdec;
    GstElement *audiotee;
    GstPad     *audioteepad;
    GstElement *audioqueue0;
    GstElement *audioqueue1;
    GstElement *audioconvert;
    GstElement *audiolevel;
    GstElement *audio_sink;
} _WatcherElements;

typedef struct _RecorderElements {
    GstElement *audioresample;
    GstElement *audiorate;
    GstElement *queue_record_video;
    GstElement *queue_record_audio;
    GstElement *video_encoder;
    GstElement *audio_encoder;
    GstElement *muxer;
    GstElement *filesink;
} _RecorderElements;

static _WatcherElements watch;
static _RecorderElements record;

static GMainLoop *loop;
static GstBus *bus;
static gboolean recording = FALSE;
static gint counter = 0;
static const char *file_path = "video";

gdouble last_rms[5];
gdouble last_average[30];
gint last_rms_id = 0;
gint last_average_id = 0;


void send_message(const std::string message) {
    //connect to messaging api
    try {
        std::string address(ACTIVEMQ_LOCATION);
        std::string user(ACTIVEMQ_USER);
        std::string password(ACTIVEMQ_PASSWORD);
        bool reconnect = false;
        bool sasl = true;
        std::string mechs = "PLAIN";
        bool insecure = true;
        messager connect(message, address, user, password, reconnect, sasl, mechs, insecure);
        proton::container(connect).run();
        
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
}

static GValue* gval_array_get_nth (GValueArray *value_array, guint index) {
    g_return_val_if_fail (value_array != NULL, NULL);
    g_return_val_if_fail (index < value_array->n_values, NULL);
    return value_array->values + index;
}

/* Dynamically link */
static void on_pad_added (GstElement *element, GstPad *pad, gpointer data){
    GstPad *sinkpad;
    GstPadLinkReturn ret;
    GstElement *decoder = (GstElement *) data;
    /* We can now link this pad with the rtsp-decoder sink pad */
    g_print ("Dynamic pad created, linking source/demuxer\n");
    sinkpad = gst_element_get_static_pad (decoder, "sink");

    /* If our converter is already linked, we have nothing to do here */
    if (gst_pad_is_linked (sinkpad)) {
        g_print("*** We are already linked ***\n");
        gst_object_unref (sinkpad);
        return;
    } else {
        g_print("proceeding to linking ...\n");
    }
    ret = gst_pad_link (pad, sinkpad);

    if (GST_PAD_LINK_FAILED (ret)) {
        //failed
        g_print("failed to link dynamically\n");
    } else {
        //pass
        g_print("dynamically link successful\n");
    }

    gst_object_unref (sinkpad);
}

/* Dynamically link */
static void on_pad_removed (GstElement *element, GstPad *pad, gpointer data){
    GstPad *sinkpad;
    GstPadLinkReturn ret;
    GstElement *decoder = (GstElement *) data;
    /* We can now link this pad with the rtsp-decoder sink pad */
    g_print ("Dynamic pad created, unlinking source/demuxer\n");
    sinkpad = gst_element_get_static_pad (decoder, "sink");

    /* If our converter is already linked, we have nothing to do here */
    if (gst_pad_is_linked (sinkpad)) {
        g_print("proceeding to unlinking ...\n");
    } else {
        g_print("*** We are already unlinked ***\n");
        gst_object_unref (sinkpad);
        return;
    }
    ret = static_cast<GstPadLinkReturn>(gst_pad_unlink(pad, sinkpad));

    if (GST_PAD_LINK_FAILED (ret)) {
        //failed
        g_print("failed to unlink dynamically\n");
    } else {
        //pass
        g_print("dynamically unlink successful\n");
    }
    gst_object_unref (sinkpad);
}

void startRecording() {
    g_print("startRecording\n");
    GstPad *videosinkpad;
    GstPad *audiosinkpad;
    GstPadTemplate *templvideo;
    GstPadTemplate *templaudio;

    /*
    gst-launch-1.0 rtspsrc location=rtsp://CAMERA_IP/Streaming/Channels/102 latency=100 name=src \
    src. ! rtph264depay ! h264parse ! avdec_h264 ! videotee name=videoteepad  \
                                                            .videoteepad ! queue ! autovideosink \
                                                            .videoteepad ! queue ! x264enc ! mp4mux ! filesink \
    src. ! rtpg726depay !  avdec_g726 !  audioconvert ! audioresample ! queue ! autoaudiosink
    */

    char *file_name = (char*) malloc(255 * sizeof(char));
    sprintf(file_name, "%s%d.mp4", file_path, counter++);
    g_print("recording to file %s\n", file_name);

    //Video elements
    templvideo = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(watch.videotee), "src_%u");
    watch.videoteepad = gst_element_request_pad(watch.videotee, templvideo, nullptr, nullptr);
    record.video_encoder = gst_element_factory_make("x264enc", nullptr);
    record.queue_record_video = gst_element_factory_make("queue", "queue_record_video0");
    if (!record.queue_record_video) {
        g_print("\nnot have queue_record_video\n");
    }
    //Audio elements
    templaudio = gst_element_class_get_pad_template(GST_ELEMENT_GET_CLASS(watch.audiotee), "src_%u");
    watch.audioteepad = gst_element_request_pad(watch.audiotee, templaudio, nullptr, nullptr);
    record.queue_record_audio = gst_element_factory_make("queue", "queue_record_audio0");
    record.audio_encoder = gst_element_factory_make("avenc_aac", nullptr);

    if (!watch.audioteepad) {
        g_print("\nnot have audioteepad\n");
    }
    if (!record.queue_record_audio) {
        g_print("\nnot have queue_record_audio\n");
    }
    if (!record.audio_encoder) {
        g_print("\nnot have audio_encoder\n");
    }

    //Muxer and Sink elements
    record.muxer = gst_element_factory_make("mp4mux", nullptr);
    record.filesink = gst_element_factory_make("filesink", nullptr);

    //Set properties
    g_object_set (G_OBJECT (record.video_encoder), "tune", 4, NULL);
    //g_object_set (G_OBJECT (record.audiorate), "parent", "audio/x-raw", NULL);
    //g_object_set (G_OBJECT (record.audiorate), "rate", 48000, NULL);
    //g_object_set (G_OBJECT (record.audiorate), "channels", 1, NULL);
    g_object_set (G_OBJECT (record.video_encoder), "bitrate", 3800, NULL);
    g_object_set (G_OBJECT (record.video_encoder), "key-int-max", 0, NULL);
    g_object_set (G_OBJECT (record.audio_encoder), "bitrate", 128000, NULL);
    g_object_set (G_OBJECT (record.muxer), "streamable", true, NULL);
    g_object_set (G_OBJECT (record.filesink), "location", file_name, NULL);
    g_object_set (G_OBJECT (record.filesink), "sync", false, NULL);

    free(file_name);

    gst_bin_add_many(GST_BIN(watch.pipeline), record.queue_record_video, record.video_encoder,
            record.queue_record_audio, record.audio_encoder,
            record.muxer, record.filesink, NULL);

    gst_element_link(record.video_encoder, record.queue_record_video);
    gst_element_link(record.queue_record_audio, record.audio_encoder);

    gst_element_link(record.queue_record_video, record.muxer);
    gst_element_link(record.audio_encoder, record.muxer);
    gst_element_link(record.muxer, record.filesink);

    gst_element_sync_state_with_parent(record.queue_record_video);
    gst_element_sync_state_with_parent(record.video_encoder);

    gst_element_sync_state_with_parent(record.queue_record_audio);
    gst_element_sync_state_with_parent(record.audio_encoder);
    gst_element_sync_state_with_parent(record.muxer);
    gst_element_sync_state_with_parent(record.filesink);

    videosinkpad = gst_element_get_static_pad(record.video_encoder, "sink");
    gst_pad_link(watch.videoteepad, videosinkpad);

    audiosinkpad = gst_element_get_static_pad(record.queue_record_audio, "sink");
    gst_pad_link(watch.audioteepad, audiosinkpad);

    gst_object_unref(videosinkpad);
    gst_object_unref(audiosinkpad);

    recording = TRUE;
}

static GstPadProbeReturn unlink_cb(GstPad *pad, GstPadProbeInfo *info, gpointer user_data) {
    g_print("unlinking...");
    GstPad *videosinkpad;
    GstPad *audiosinkpad;
    videosinkpad = gst_element_get_static_pad (record.queue_record_video, "sink");
    gst_pad_unlink (watch.videoteepad, videosinkpad);
    gst_object_unref (videosinkpad);

    audiosinkpad = gst_element_get_static_pad (record.queue_record_audio, "sink");
    gst_pad_unlink (watch.audioteepad, audiosinkpad);
    gst_object_unref (audiosinkpad);

    gst_element_send_event(record.video_encoder, gst_event_new_eos());
    gst_element_send_event(record.audio_encoder, gst_event_new_eos());

    sleep(2);
    gst_element_set_state(record.video_encoder, GST_STATE_NULL);
    gst_element_set_state(record.queue_record_video, GST_STATE_NULL);

    gst_element_set_state(record.audio_encoder, GST_STATE_NULL);
    gst_element_set_state(record.queue_record_audio, GST_STATE_NULL);

    gst_element_set_state(record.muxer, GST_STATE_NULL);
    gst_element_set_state(record.filesink, GST_STATE_NULL);

    gst_bin_remove(GST_BIN (watch.pipeline), record.video_encoder);
    gst_bin_remove(GST_BIN (watch.pipeline), record.queue_record_video);

    gst_bin_remove(GST_BIN (watch.pipeline), record.audio_encoder);
    gst_bin_remove(GST_BIN (watch.pipeline), record.queue_record_audio);

    gst_bin_remove(GST_BIN (watch.pipeline), record.muxer);
    gst_bin_remove(GST_BIN (watch.pipeline), record.filesink);

    gst_element_release_request_pad (watch.videotee, watch.videoteepad);
    gst_object_unref (watch.videoteepad);

    gst_element_release_request_pad (watch.audiotee, watch.audioteepad);
    gst_object_unref (watch.audioteepad);

    g_print("unlinked\n");

    return GST_PAD_PROBE_REMOVE;
}

void stopRecording() {
    g_print("stopRecording\n");
    gst_pad_add_probe(watch.videoteepad, GST_PAD_PROBE_TYPE_IDLE, unlink_cb, NULL, (GDestroyNotify) g_free);
    recording = FALSE;
}

gdouble getAverage(gint size) {
    gint i;
    gfloat sum = 0;
    gfloat avg;
    for (i = 0; i < size; ++i) {
        sum += last_rms[i];
    }
    avg = gdouble(sum) / size;
    return avg;
}

static gboolean bus_call (GstBus *bus,GstMessage *msg, gpointer data) {
    GMainLoop *loop = (GMainLoop *) data;
    switch (GST_MESSAGE_TYPE (msg)) {
        case GST_MESSAGE_EOS:
            g_print ("Stream Ends\n");
            g_main_loop_quit (loop);
            break;
        case GST_MESSAGE_ERROR: {
            gchar  *debug;
            GError *error;
            gst_message_parse_error (msg, &error, &debug);
            g_free (debug);
            g_printerr ("Error: %s\n", error->message);
            g_error_free (error);
            g_main_loop_quit (loop);
            break;
        }
        case GST_MESSAGE_ELEMENT: {
            const GstStructure *s = gst_message_get_structure(msg);
            const gchar *name = gst_structure_get_name(s);

            if (strcmp(name, "level") == 0) {
                gint channels;
                GstClockTime endtime;
                gdouble rms_dB, peak_dB, decay_dB;
                gdouble rms;
                const GValue *array_val;
                const GValue *value;
                GValueArray *rms_arr, *peak_arr, *decay_arr;
                gint i;

                if (!gst_structure_get_clock_time(s, "endtime", &endtime))
                    g_warning ("Could not parse endtime");

                /* the values are packed into GValueArrays with the value per channel */
                array_val = gst_structure_get_value(s, "rms");
                rms_arr = (GValueArray *) g_value_get_boxed(array_val);

                array_val = gst_structure_get_value(s, "peak");
                peak_arr = (GValueArray *) g_value_get_boxed(array_val);

                array_val = gst_structure_get_value(s, "decay");
                decay_arr = (GValueArray *) g_value_get_boxed(array_val);

                /* we can get the number of channels as the length of any of the value
                 * arrays */

                //g_print("endtime: %" GST_TIME_FORMAT, GST_TIME_ARGS (endtime));

                value = gval_array_get_nth(rms_arr, 0);
                rms_dB = g_value_get_double(value);

                rms = pow(10, rms_dB / 20);
                //g_print("  normalized rms value: %f\n", rms);

                last_rms[last_rms_id] = rms;
                last_rms_id += 1;

                gint arr_size = 5;
                if (last_rms_id >= arr_size) {
                    last_rms_id = 0;
                    gfloat avr = getAverage(arr_size);


                    last_average[last_average_id] = avr;
                    last_average_id += 1;

                    //g_print("average : %f\n", avr);
                    if (avr < RECORDING_AUDIO_RMS && recording) {
                        bool silence = true;
                        int n;
                        for ( n=0 ; n<30 ; ++n )
                        {
                            if (last_average[n] >= RECORDING_AUDIO_RMS) {
                                silence = false;
                                break;
                            }
                        }
                        if (silence) {
                            std::string message = "{\"type\": \"notification\", \"message\":\"stop recording\", \"average\":\"" + std::to_string(avr) + "\"}";
                            send_message(message);
                            stopRecording();
                        }
                    } else if (avr >= RECORDING_AUDIO_RMS && !recording) {
                        std::string message = "{\"type\": \"alert\", \"message\":\"start recording\", \"average\":\"" + std::to_string(avr) + "\"}";
                        send_message(message);
                        startRecording();
                    }
                    if (last_average_id >= 30) {
                        last_average_id = 0;
                        if (recording) {
                            std::string message = "{\"type\": \"notification\", \"message\":\"continue recording\", \"average\":\"" + std::to_string(avr) + "\"}";
                            send_message(message);
                        }
                    }
                }
            }
        }
        default:
            break;
    }
    return TRUE;
}

int sigintHandler() {
    g_print("You ctrl-c!\n");
    if (recording){
        stopRecording();
        exit(0);
    }
    return 0;
}

int main(int argc, char *argv[])
{
    //signal(SIGINT, reinterpret_cast<void (*)(int)>(sigintHandler));

    /* Initializing GStreamer */
    gst_init (&argc, &argv);
    loop = g_main_loop_new (NULL, FALSE);

    /*
gst-launch-1.0 rtspsrc location=rtsp://CAMERA_IP/Streaming/Channels/102 latency=100 name=src \
src. ! rtph264depay ! h264parse ! avdec_h264 ! videotee name=videoteepad videoteepad. ! queue ! autovideosink \
src. ! rtppcmadepay ! decodebin ! queue !  audioconvert ! audioresample ! queue ! autoaudiosink
    */


    /* Create Pipe's Elements */
    watch.pipeline = gst_pipeline_new ("video player");
    watch.rtspsrc = gst_element_factory_make ("rtspsrc", "rtspsrc0");

    //Video elements
    watch.rtph264depay = gst_element_factory_make ("rtph264depay", "rtph264depay0");
    watch.h264parse = gst_element_factory_make ("h264parse", "h264parse0");
    watch.avdec_h264 = gst_element_factory_make ("decodebin", "avdec_h2640");
    watch.videotee = gst_element_factory_make ("tee", "videotee0");
    watch.videoqueue0 = gst_element_factory_make ("queue", "videoqueue0");
    watch.videoconvert = gst_element_factory_make ("videoconvert", "videoconvert0");

    //Video Sink
    watch.video_sink = gst_element_factory_make ("fakesink", "autovideosink0");

    //video
    g_object_set (G_OBJECT (watch.video_sink), "sync", FALSE, NULL);

    //Audio Elements
    watch.rtppcmadepay = gst_element_factory_make ("rtppcmadepay", "rtppcmadepay0");
    watch.alawdec = gst_element_factory_make ("decodebin", "alawdec0");
    watch.audioqueue0 = gst_element_factory_make ("queue", "audioqueue0");
    watch.audioconvert = gst_element_factory_make ("audioconvert", "audioconvert0");
    watch.audiolevel = gst_element_factory_make ("level", "audiolevel0");
    watch.audiotee = gst_element_factory_make ("tee", "audiotee0");
    watch.audioqueue1 = gst_element_factory_make ("queue", "audioqueue1");

    //Audio Sink
    watch.audio_sink = gst_element_factory_make ("fakesink", "autoaudiosink0");
    g_object_set (G_OBJECT (watch.audio_sink), "sync", FALSE, NULL);
    //g_object_set (G_OBJECT (audio_sink), "async-handling", TRUE, NULL);

    /* Set video Source */
    g_object_set (G_OBJECT (watch.rtspsrc), "location", RTSP_LOCATION, NULL);
    g_object_set (G_OBJECT (watch.rtspsrc), "user-id", RTSP_USER, NULL);
    g_object_set (G_OBJECT (watch.rtspsrc), "user-pw", RTSP_PASSWORD, NULL);
    g_object_set (G_OBJECT (watch.rtspsrc), "do-rtcp", TRUE, NULL);
    g_object_set (G_OBJECT (watch.rtspsrc), "latency", RTSP_LATENCY, NULL);

    /* Putting a Message handler */
    bus = gst_pipeline_get_bus (GST_PIPELINE (watch.pipeline));
    gst_bus_add_watch (bus, bus_call, loop);
    gst_object_unref (bus);

    if (!watch.pipeline) {
        gst_print("error 1\n");
    }
    if (!watch.rtspsrc) {
        gst_print("error 2\n");
    }
    if (!watch.rtph264depay) {
        gst_print("error 3\n");
    }
    if (!watch.h264parse) {
        gst_print("error 4\n");
    }
    if (!watch.avdec_h264) {
        gst_print("error 5\n");
    }
    if (!watch.videoqueue0) {
        gst_print("error 6\n");
    }
    if (!watch.videotee) {
        gst_print("error 7\n");
    }
    if (!watch.videoconvert) {
        gst_print("error 8\n");
    }
    if (!watch.rtppcmadepay) {
        gst_print("error 9\n");
    }
    if (!watch.alawdec) {
        gst_print("error 10\n");
    }
    if (!watch.audiotee) {
        gst_print("error 11\n");
    }
    if (!watch.audioqueue0) {
        gst_print("error 12\n");
    }
    if (!watch.audioconvert) {
        gst_print("error 13\n");
    }
    if (!watch.audiolevel) {
        gst_print("error 14\n");
    }
    if (!watch.audioqueue1) {
        gst_print("error 15\n");
    }
    if (!watch.audio_sink) {
        gst_print("error 16\n");
    }


    //Make sure: Every elements was created ok
    if (!watch.pipeline || !watch.rtspsrc || !watch.rtph264depay || !watch.h264parse || !watch.avdec_h264
        || !watch.videoqueue0 || !watch.videotee || !watch.videoconvert || !watch.rtppcmadepay || !watch.alawdec
        || !watch.audiotee || !watch.audioqueue0 || !watch.audioconvert || !watch.audiolevel || !watch.audioqueue1
        || !watch.audio_sink) {
        g_printerr ("One of the elements wasn't created... Exiting\n");
        return -1;
    }

    // Add Elements to the Bin
    gst_bin_add_many (GST_BIN (watch.pipeline), watch.rtspsrc, watch.rtph264depay, watch.h264parse,
                      watch.avdec_h264, watch.videotee, watch.videoqueue0, watch.videoconvert, watch.video_sink,
                      watch.rtppcmadepay, watch.alawdec, watch.audiotee, watch.audioqueue0, watch.audioconvert,
                      watch.audiolevel, watch.audioqueue1, watch.audio_sink, NULL);

    // Link confirmation
    if (!gst_element_link_many (watch.rtph264depay, watch.h264parse, watch.avdec_h264, NULL)){
        g_warning ("Linking part (A)-1 Fail...");
        return -2;
    }
    //Linking order is important -> start linking all elements from left (near rtspsrc) to right (near videosink)
    // Link confirmation
    if (!gst_element_link_many (watch.rtppcmadepay, watch.alawdec, NULL)){
        g_warning ("Linking part (B)-1 Fail...");
        return -3;
    }
    // Link confirmation
    if (!gst_element_link_many (watch.videotee, watch.videoqueue0, watch.videoconvert, watch.video_sink, NULL)){
        g_warning ("Linking part (A)-2 Fail...");
        return -4;
    }
    // Link confirmation
    if (!gst_element_link_many (watch.audioqueue0, watch.audioconvert, watch.audiolevel, NULL)){
        g_warning ("Linking part (B)-3 Fail...");
        return -5;
    }

    // Link confirmation
    if (!gst_element_link (watch.audioqueue1, watch.audio_sink)){
        g_warning ("Linking part (B)-4 Fail...");
        return -5;
    }

    // Link confirmation
    if (!gst_element_link (watch.audiotee, watch.audioqueue1)){
        g_warning ("Linking part (B)-5 Fail...");
        return -5;
    }

    // Link confirmation
    if (!gst_element_link (watch.audiolevel, watch.audiotee)){
        g_warning ("Linking part (B)-6 Fail...");
        return -5;
    }

    // Dynamic Pad Creation
    if(! g_signal_connect (watch.rtspsrc, "pad-added", G_CALLBACK (on_pad_added), watch.rtph264depay))
    {
        g_warning ("Linking part (1) with part (A)-1 Fail...");
    }

    // Dynamic Pad Creation
    if(! g_signal_connect (watch.avdec_h264, "pad-added", G_CALLBACK (on_pad_added), watch.videotee))
    {
        g_warning ("Linking part (2) with part (A)-2 Fail...");
    }

    // Dynamic Pad Creation
    if(! g_signal_connect (watch.rtspsrc, "pad-added", G_CALLBACK (on_pad_added), watch.rtppcmadepay))
    {
        g_warning ("Linking part (1) with part (B)-1 Fail...");
    }

    // Dynamic Pad Creation
    if(! g_signal_connect (watch.alawdec, "pad-added", G_CALLBACK (on_pad_added), watch.audioqueue0))
    {
        g_warning ("Linking part (2) with part (B)-2 Fail...");
    }
    GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN (watch.pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "pipeline_before_rec");
    //startRecording();

    /* Run the pipeline */
    g_print ("Playing: %s\n", argv[1]);
    gst_element_set_state (watch.pipeline, GST_STATE_PLAYING);
    GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN (watch.pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "pipeline_after_playing");
    g_main_loop_run (loop);

    /* Ending Playback */
    g_print ("End of the Streaming... ending the playback\n");
    gst_element_set_state (watch.pipeline, GST_STATE_NULL);

    /* Eliminating Pipeline */
    g_print ("Eliminating Pipeline\n");
    gst_object_unref (GST_OBJECT (watch.pipeline));

    return 0;
}
