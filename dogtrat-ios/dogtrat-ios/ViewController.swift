//
//  ViewController.swift
//  dogtrat-ios
//
//  Created by Alexis RIES on 19/02/2020.
//  Copyright © 2020 Alexis RIES. All rights reserved.
//

import AudioToolbox
import UIKit
import Foundation


class ViewController: UIViewController, StompClientLibDelegate {
    @IBOutlet weak var label1: UILabel!
    var socketClient = StompClientLib()
    let topic = "/topic/dogtrat/notifications"
    var url = NSURL()

    @IBAction func vibrationCourte(_ sender: UIButton) {
        launchAction(actionId: 1)
    }
    
    @IBAction func vibrationLongue(_ sender: UIButton) {
        launchAction(actionId: 2)
    }
    
    @IBAction func stimulationCourte(_ sender: UIButton) {
        launchAction(actionId: 3)
    }
    
    @IBAction func stimulationLongue(_ sender: UIButton) {
        launchAction(actionId: 4)
    }
    @IBAction func stimulatiodnLongue(_ sender: UIButton) {
        launchAction(actionId: 4)
    }
    
    
    let mediaPlayer = VLCMediaPlayer()
    var ispaused = false
    
    func playURI(uri: String) {
        mediaPlayer.drawable = self.imageView
        let url = URL(string: uri)
        let media = VLCMedia(url: url!)
        media.addOptions(["network-caching": 300])
        mediaPlayer.media = media
        mediaPlayer.play()
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var videoButton: UIImageView!
    
    
    @objc func tapDetected() {
        if ispaused {
            self.ispaused = false
            mediaPlayer.play()
        } else {
            self.ispaused = true
            mediaPlayer.stop()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        registerSocket()
        
        UIApplication.shared.isIdleTimerDisabled = true
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(singleTap)
 
        playURI(uri: "rtsp://dogtrat:CAMPASSWORD@yourdomain.ddns.net:33554/Streaming/Channels/102")
    }
    

    
    func pauseVideo() {
        videoButton.isHidden = true
        mediaPlayer.stop()
    }

    func launchAction(actionId: Int) {
        let actions = [
            1: "vibration courte",
            2: "vibration longue",
            3: "stimulation courte",
            4: "stimulation longue"
        ]

        let alert = UIAlertController(title: "Confirmation", message: "Êtes vous sûr de vouloir envoyer une \(actions[actionId]!) ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Non", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Oui", style: .destructive, handler:{ action in
            self.socketClient.sendMessage(message: "{\"action\": \(actionId)}", toDestination: "/topic/dogtrat/command", withHeaders: nil, withReceipt: nil)
            //NetworkManager().sendcommand(action: actionId)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func registerSocket(){
        let baseURL = "mq.yourdomain.fr:31614"
        let completedWSURL = "wss://\(baseURL)/api/websocket"
        
        url = NSURL(string: completedWSURL)!
        socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate)
    }
    
    func stompClientDidConnect(client: StompClientLib!) {
        let topic = self.topic
        print("Socket is Connected : \(topic)")
        socketClient.subscribe(destination: topic)
        // Auto Disconnect after 15 sec
        socketClient.autoDisconnect(time: 15)
        // Reconnect after 2 sec
        socketClient.reconnect(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate, time: 2.0)
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        print("Socket is Disconnected")
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, akaStringBody stringBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        print("DESTINATION : \(destination)")
        //print("JSON BODY : \(String(describing: jsonBody))")
        print("STRING BODY : \(stringBody ?? "nil")")
        let jbody = jsonBody as? [String:Any]
        let type = jbody!["type"]! as? String
        let message = jbody!["message"]! as? String
        let average = jbody!["average"] as? String
        self.label1.text = "\(getDate()) \(message!) - avr : \(average!)"
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        
        print("TYPE : \(String(describing: type!) )")
        print("MESSAGE : \(String(describing: message!) )")
        print("AVERAGE : \(String(describing: average!) )")
    }
    
    func stompClientJSONBody(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        print("DESTIONATION : \(destination)")
        print("String JSON BODY : \(String(describing: jsonBody))")
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        print("Receipt : \(receiptId)")
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        print("Error : \(String(describing: message))")
        self.label1.text = "Erreur websocket"
    }
    
    func serverDidSendPing() {
        print("Server Ping")
    }
    
    func getDate() -> String {
        let date = Date()
        var calendar = Calendar.current
        //print(calendar.dateComponents([.year, .month, .day, .hour, .minute], from: date))
        calendar.timeZone = TimeZone(identifier: "Europe/Paris")!
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        return "\(hour):\(minutes):\(seconds)"
    }
}
